public class Wagon {
	int klasse;

	public int getKlasse() {
		return klasse;
	}

	public void setKlasse(int klasse) {
		this.klasse = klasse;
	}

	public Wagon(int klasse) {
		super();
		this.klasse = klasse;
	}
}
