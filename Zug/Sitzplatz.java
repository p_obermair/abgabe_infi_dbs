public class Sitzplatz {
	int wagon;

	public Sitzplatz(int wagon) {
		super();
		this.wagon = wagon;
	}

	public int getWagon() {
		return wagon;
	}

	public void setWagon(int wagon) {
		this.wagon = wagon;
	}

}
