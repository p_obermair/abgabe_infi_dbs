import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class GUI extends JFrame {

	private JPanel contentPane;
	private JTextField txtFieldKundeName;
	private JTextField txtFieldKundeSitzplatz;
	private JTextField txtFieldWagonKlasse;
	private JTextField txtFieldWagonWagon;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUI frame = new GUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public GUI() {
		setTitle("Zugverwaltung");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 573, 363);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel KundeAnlegenLabel = new JLabel("Kunde Anlegen:");
		KundeAnlegenLabel.setFont(new Font("Calibri", Font.PLAIN, 14));
		KundeAnlegenLabel.setBounds(10, 254, 88, 14);
		contentPane.add(KundeAnlegenLabel);
		
		JLabel KundeAnLable = new JLabel("Name:");
		KundeAnLable.setFont(new Font("Calibri", Font.PLAIN, 12));
		KundeAnLable.setBounds(10, 279, 61, 14);
		contentPane.add(KundeAnLable);
		
		txtFieldKundeName = new JTextField();
		txtFieldKundeName.setFont(new Font("Calibri", Font.PLAIN, 12));
		txtFieldKundeName.setBounds(55, 276, 86, 20);
		contentPane.add(txtFieldKundeName);
		txtFieldKundeName.setColumns(10);
		
		JLabel KundeAnLabel = new JLabel("Sitzplatz:");
		KundeAnLabel.setFont(new Font("Calibri", Font.PLAIN, 12));
		KundeAnLabel.setBounds(168, 279, 61, 14);
		contentPane.add(KundeAnLabel);
		
		txtFieldKundeSitzplatz = new JTextField();
		txtFieldKundeSitzplatz.setFont(new Font("Calibri", Font.PLAIN, 12));
		txtFieldKundeSitzplatz.setBounds(226, 276, 86, 20);
		contentPane.add(txtFieldKundeSitzplatz);
		txtFieldKundeSitzplatz.setColumns(10);
		
		JButton ButtonKundeAnlegen = new JButton("anlegen");
		ButtonKundeAnlegen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String k = txtFieldKundeName.getText();
				int k2 = Integer.parseInt(txtFieldKundeSitzplatz.getText());
				Kunde ku = new Kunde(k,k2);
				DBManager.kundeAnlegen(ku);
				
			}
		});
		ButtonKundeAnlegen.setFont(new Font("Calibri", Font.PLAIN, 12));
		ButtonKundeAnlegen.setBounds(342, 275, 89, 23);
		contentPane.add(ButtonKundeAnlegen);
		
		JLabel WagonAnlegenLabel = new JLabel("Wagon Anlegen:");
		WagonAnlegenLabel.setFont(new Font("Calibri", Font.PLAIN, 14));
		WagonAnlegenLabel.setBounds(10, 28, 118, 14);
		contentPane.add(WagonAnlegenLabel);
		
		JLabel WagonAnLabel = new JLabel("KLasse:");
		WagonAnLabel.setFont(new Font("Calibri", Font.PLAIN, 12));
		WagonAnLabel.setBounds(10, 53, 46, 14);
		contentPane.add(WagonAnLabel);
		
		txtFieldWagonKlasse = new JTextField();
		txtFieldWagonKlasse.setFont(new Font("Calibri", Font.PLAIN, 12));
		txtFieldWagonKlasse.setBounds(55, 49, 23, 20);
		contentPane.add(txtFieldWagonKlasse);
		txtFieldWagonKlasse.setColumns(10);
		
		JLabel WagonHinweisLabel = new JLabel("1/2 f\u00FCr die Klasse angeben!");
		WagonHinweisLabel.setFont(new Font("Calibri", Font.PLAIN, 12));
		WagonHinweisLabel.setBounds(88, 53, 165, 14);
		contentPane.add(WagonHinweisLabel);
		
		JLabel SitzplatzAnlegenLabel = new JLabel("Sitzplatz Anlegen:");
		SitzplatzAnlegenLabel.setFont(new Font("Calibri", Font.PLAIN, 14));
		SitzplatzAnlegenLabel.setBounds(10, 142, 118, 14);
		contentPane.add(SitzplatzAnlegenLabel);
		
		JLabel SitzplatzAnlegenWagon = new JLabel("Wagon:");
		SitzplatzAnlegenWagon.setFont(new Font("Calibri", Font.PLAIN, 12));
		SitzplatzAnlegenWagon.setBounds(10, 167, 46, 14);
		contentPane.add(SitzplatzAnlegenWagon);
		
		txtFieldWagonWagon = new JTextField();
		txtFieldWagonWagon.setFont(new Font("Calibri", Font.PLAIN, 12));
		txtFieldWagonWagon.setBounds(55, 164, 29, 20);
		contentPane.add(txtFieldWagonWagon);
		txtFieldWagonWagon.setColumns(10);
		
		JButton ButtonSittzplatzAnlgene = new JButton("anlegen");
		ButtonSittzplatzAnlgene.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int s = Integer.parseInt(txtFieldWagonWagon.getText());
				Sitzplatz s1 = new Sitzplatz(s);
				DBManager.sitzplatzAnlegen(s1);
				
			}
		});
		ButtonSittzplatzAnlgene.setFont(new Font("Calibri", Font.PLAIN, 12));
		ButtonSittzplatzAnlgene.setBounds(114, 163, 89, 23);
		contentPane.add(ButtonSittzplatzAnlgene);
		
		JButton ButtonWagonAnlgene = new JButton("anlegen");
		ButtonWagonAnlgene.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int k = Integer.parseInt(txtFieldWagonKlasse.getText());
				Wagon w = new Wagon(k);
				DBManager.wagonAnlegen(w);
				
			}
		});
		ButtonWagonAnlgene.setFont(new Font("Calibri", Font.PLAIN, 12));
		ButtonWagonAnlgene.setBounds(263, 48, 89, 23);
		contentPane.add(ButtonWagonAnlgene);
	}
}
