public class Kunde {
	String name;
	int sitzplatz;

	public Kunde(String name, int sitzplatz) {
		super();
		this.name = name;
		this.sitzplatz = sitzplatz;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getSitzplatz() {
		return sitzplatz;
	}

	public void setSitzplatz(int sitzplatz) {
		this.sitzplatz = sitzplatz;
	}
}
