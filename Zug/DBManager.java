import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DBManager {
	public static void main(String[] args) {
		tableKunde();
		tableSitzplatz();
		tableWagon();
		GUI g = new GUI();
		g.setVisible(true);

	}

	public static void tableKunde() {
		Connection c = null;
		Statement stmt = null;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:C:/Schule/4AHWII/INFI-DBS/Zug_Verwaltung/zug.db");
			stmt = c.createStatement();
			String sql = "CREATE TABLE IF NOT Exists Kunde " + "(KundenID INTEGER PRIMARY KEY AUTOINCREMENT,"
					+ "Name STRING NOT NULL," + "Sitzplatz INT NOT NULL,"
					+ "FOREIGN KEY (Sitzplatz) references Sitzplatz(SitzplatzID));";
			stmt.executeUpdate(sql);
			stmt.close();
			c.close();
		} catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}
	}

	public static void tableSitzplatz() {
		Connection c = null;
		Statement stmt = null;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:C:/Schule/4AHWII/INFI-DBS/Zug_Verwaltung/zug.db");
			stmt = c.createStatement();
			String sql = "CREATE TABLE IF NOT Exists Sitzplatz " + "(SitzplatzID INTEGER PRIMARY KEY AUTOINCREMENT,"
					+ "Wagon INT NOT NULL," + "FOREIGN KEY (Wagon) references Wagon(WagonID));";
			stmt.executeUpdate(sql);
			stmt.close();
			c.close();
		} catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}
	}

	public static void tableWagon() {
		Connection c = null;
		Statement stmt = null;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:C:/Schule/4AHWII/INFI-DBS/Zug_Verwaltung/zug.db");
			stmt = c.createStatement();
			String sql = "CREATE TABLE IF NOT Exists Wagon " + "(WagonID INTEGER PRIMARY KEY AUTOINCREMENT,"
					+ "Klasse INT NOT NULL);";
			stmt.executeUpdate(sql);
			stmt.close();
			c.close();
		} catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}
	}

	public static void kundeAnlegen(Kunde kunde) {
		Connection c = null;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:C:/Schule/4AHWII/INFI-DBS/Zug_Verwaltung/zug.db");
			String sql = "INSERT INTO Kunde Values(?,?,?);";
			PreparedStatement stmt = c.prepareStatement(sql);
			stmt.setString(2, kunde.getName());
			stmt.setInt(3, kunde.getSitzplatz());
			stmt.executeUpdate();
			stmt.close();
			c.close();
		} catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}

	}

	public static void wagonAnlegen(Wagon wagon) {
		Connection c = null;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:C:/Schule/4AHWII/INFI-DBS/Zug_Verwaltung/zug.db");
			String sql = "INSERT INTO Wagon Values(?,?);";
			PreparedStatement stmt = c.prepareStatement(sql);
			stmt.setInt(2, wagon.getKlasse());
			stmt.executeUpdate();
			stmt.close();
			c.close();
		} catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}

	}

	public static void sitzplatzAnlegen(Sitzplatz sitzplatz) {
		Connection c = null;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:C:/Schule/4AHWII/INFI-DBS/Zug_Verwaltung/zug.db");
			String sql = "INSERT INTO Sitzplatz Values(?,?);";
			PreparedStatement stmt = c.prepareStatement(sql);
			stmt.setInt(2, sitzplatz.getWagon());
			stmt.executeUpdate();
			stmt.close();
			c.close();
		} catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}

	}
	private void summeErste() throws ClassNotFoundException {
		PreparedStatement prestmt = null;
		ResultSet rs = null;
		Connection c = null;
		Statement stmt = null;

		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:C:/Schule/4AHWII/INFI-DBS/Zug_Verwaltung/zug.db");
			stmt = c.createStatement();
			String sql = "SELECT COUNT(KundenID) FROM Wagon WHERE Klasse = 1;";
			prestmt = c.prepareStatement(sql);
			rs = prestmt.executeQuery();
			System.out.println("Gesamt: " + rs.getInt(1));
		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			try {
				prestmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}

		}
	}
	private void summeZweite() throws ClassNotFoundException {
		PreparedStatement prestmt = null;
		ResultSet rs = null;
		Connection c = null;
		Statement stmt = null;

		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:C:/Schule/4AHWII/INFI-DBS/Zug_Verwaltung/zug.db");
			stmt = c.createStatement();
			String sql = "SELECT COUNT(KundenID) FROM Wagon WHERE Klasse = 2;";
			prestmt = c.prepareStatement(sql);
			rs = prestmt.executeQuery();
			System.out.println("Gesamt: " + rs.getInt(1));
		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			try {
				prestmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}

		}
	}
	private void summeLeuteZug() throws ClassNotFoundException {
		PreparedStatement prestmt = null;
		ResultSet rs = null;
		Connection c = null;
		Statement stmt = null;

		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:C:/Schule/4AHWII/INFI-DBS/Zug_Verwaltung/zug.db");
			stmt = c.createStatement();
			String sql = "SELECT COUNT(KundenID) FROM Kunde";
			prestmt = c.prepareStatement(sql);
			rs = prestmt.executeQuery();
			System.out.println("Gesamt: " + rs.getInt(1));
		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			try {
				prestmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}

		}
	}
	
}
