import static org.junit.Assert.*;

import org.junit.Test;

public class PokerTest 
{
	@Test
	public void einPaarTest() 
	{
		int[] gez = {13,26,3,4,17};
		boolean result = Auswertung.einPaar(gez);
		assertEquals(true, result);
	}
	@Test
	public void zweiPaarTest() 
	{
		int[] gez = {13,26,3,16,17};
		boolean result = Auswertung.zweiPaar(gez);
		assertEquals(true, result);
	}
	@Test
	public void drillingTest() 
	{
		int[] gez = {12,26,25,16,51};
		boolean result = Auswertung.drilling(gez);
		assertEquals(true, result);
	}
	@Test
	public void vierlingTest() 
	{
		int[] gez = {4,30,0,16,43};
		boolean result = Auswertung.vierling(gez);
		assertEquals(true, result);
	}
	@Test
	public void flushTest() 
	{
		int[] gez = {2,6,10,8,12};
		boolean result = Auswertung.flush(gez);
		assertEquals(true, result);
	}
	@Test
	public void fullHouseTest() 
	{
		int[] gez = {0,3,13,26,43};
		boolean result = Auswertung.fullHouse(gez);
		assertEquals(true, result);
	}
	@Test
	public void straightTest() 
	{
		int[] gez = {4,18,32,46,3};
		boolean result = Auswertung.straight(gez);
		assertEquals(true, result);
	}
	@Test
	public void straightFlushTest() 
	{
		int[] gez = {5,6,7,8,9};
		boolean result = Auswertung.straightFlush(gez);
		assertEquals(true, result);
	}

}
