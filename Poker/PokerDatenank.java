import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class PokerDatenank 
{
	public static void main(String[] args)
	{
		System.out.println(t);
	}
	static int[] e = new int[8];
	static int trys = 0;
	static long t = 0;
	public static void database()
	{
		e = Auswertung.getz();
		trys = Auswertung.getversuche();
		t = System.currentTimeMillis() / 1000;
		
		Connection c = null;
		Statement stmt = null;
		try 
		{
			Class.forName("org.sqlite.JDBC");
		    c = DriverManager.getConnection("jdbc:sqlite:PokerDatenbank.db");
		    stmt = c.createStatement();
		    String sql = "CREATE TABLE IF NOT Exists Poker " +
		                   "(Unix INT PRIMARY KEY NOT NULL," +
		                   "versuche int NOT NULL, " + 
		                   "einPaar int NOT NULL, " + 
		                   "zweiPaar int NOT NULL, " + 
		                   "drilling int NOT NULL, " +
		                   "vierling int NOT NULL, " +
		                   "flush int NOT NULL, " +
		                   "fullHouse int NOT NULL, " +
		                   "straight int NOT NULL, " +
		                   "straightFlush int NOT NULL);"; 
		      stmt.executeUpdate(sql); 
		      stmt.close();
		      c.close();
		} catch ( Exception e ) 
		  {
		    System.err.println( e.getClass().getName() + ": " + e.getMessage() );
		    System.exit(0);
		  }
	}
	public static void insert()
	{
		Connection c = null;
		Statement stmt = null;
		try
		{
		Class.forName("org.sqlite.JDBC");
	    c = DriverManager.getConnection("jdbc:sqlite:PokerDatenbank.db");
	    stmt = c.createStatement();
	    
	    String sql = "INSERT INTO Poker (Unix,versuche,einPaar,zweiPaar,drilling,vierling,flush,fullHouse,straight,straightFlush) " +
	            "VALUES (" + t + "," + trys + "," + e[0] + "," + e[1] + "," + e[2] + ","
	    		 + e[3] + "," + e[4] + "," + e[5] + "," + e[6] + "," + e[7] + ");"; 
	    stmt.executeUpdate(sql);
	    stmt.close();
		}catch (Exception e)
		{
			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
		    System.exit(0);
		}
		
	}
	public static void check()
	{
		 Connection c = null;
		 Statement stmt = null;
		 try {
		        Class.forName("org.sqlite.JDBC");
		        c = DriverManager.getConnection("jdbc:sqlite:PokerDatenbank.db");
		        c.setAutoCommit(false);

		        stmt = c.createStatement();
		        ResultSet rs = stmt.executeQuery( "SELECT * FROM Poker;" );
		        while ( rs.next() ) {
		        int t = rs.getInt("Unix");
		        int tr = rs.getInt("versuche");
		        int e = rs.getInt("einPaar");
		        int z = rs.getInt("zweiPaar");
		        int d = rs.getInt("drilling");
		        int v = rs.getInt("vierling");
		        int f = rs.getInt("flush");
		        int fh = rs.getInt("fullHouse");
		        int s = rs.getInt("straight");
		        int sf = rs.getInt("straightFlush");
		        System.out.println(" ");
		        System.out.println( "Unix: " + t );
		        System.out.println( "Versuche: " + tr);
	            System.out.println( "einPaar: " + e );
                System.out.println( "zweiPaar: " + z);
	            System.out.println( "drilling: " + d);
	            System.out.println( "vierling: " + v);
	            System.out.println( "flush: " + f);
	            System.out.println( "fullHouse: " + fh);
	            System.out.println( "straight: " + s);
	            System.out.println( "straightFlush: " + sf);
		      }
		      rs.close();
		      stmt.close();
		      c.close();
		    } catch ( Exception e ) {
		      System.err.println( e.getClass().getName() + ": " + e.getMessage() );
		      System.exit(0);
	}
	}}
