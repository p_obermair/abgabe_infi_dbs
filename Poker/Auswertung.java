import java.util.Scanner;

public class Auswertung 
{
	static final int ANZAHLGLEICHERFARBEN = 13;
	static final int ANZAHLFARBEN = 4;
	
	static int[] gez = new int[5];
	static int[] z = new int[8];
	static int[] t = {1,2,3};
	static int versuche;
	static double b;
	static double e;
	
	public static void main(String[] args) 
	{
		System.out.print("Bitte geben Sie die Anzahl der Versuche ein: ");
		Scanner s = new Scanner(System.in);
		versuche = s.nextInt();
		b = System.currentTimeMillis();
		
		for(int i = 0; i <= versuche; i++)
		{
			arrayErstellen();
			if(einPaar(gez) == true)
			{
				z[0]++;
			}
			if(zweiPaar(gez) == true)
			{
				z[1]++;
			}
			if(drilling(gez) == true)
			{
				z[2]++;
			}
			if(vierling(gez) == true)
			{
				z[3]++;
			}
			if(flush(gez) == true)
			{
				z[4]++;
			}
			if(fullHouse(gez) == true)
			{
				z[5]++;
			}
			if(straight(gez) == true)
			{
				z[6]++;
			}
			if(straightFlush(gez) == true)
			{
				z[7]++;
			}
		}
		ausgabe();
	}
	public static void arrayErstellen()
	{
		int[] karten = new int[ANZAHLGLEICHERFARBEN*ANZAHLFARBEN];
		for(int i = 0; i<karten.length; i++)
		{
			karten[i] = i;
		}
		
		for(int i = 0; i<5;i++)
		{
			
			int karte = (int) (Math.random() * (52-i));
			gez[i] = karten[karte];
			
			int temp = karten[51-i];
			karten[karte]=karten[51-i];
			karten[karte]=temp;
		}
	}
	
	public static int wahrerWertDerKarte(int karte)
	{
		return karte % ANZAHLGLEICHERFARBEN;
	}
	public static int wahreFarbeDerKarte(int karte)
	{
		return karte / ANZAHLFARBEN;
	}
	//Methoden:
	
	public static boolean einPaar(int[] gez)
	{
		for(int i =0; i<gez.length-1;i++)
		{
			for(int j= i+1; j<gez.length; j++)
			{
				if(wahrerWertDerKarte(gez[i])==wahrerWertDerKarte(gez[j]))
				{
					return true;
				}
			}
		}
		return false;
	}
	public static boolean zweiPaar(int gez[])
	{
		int c = 0;
		for(int i = 0; i<gez.length-1; i++)
		{
			for(int j = i+1; j<gez.length;j++)
			{
				if(wahrerWertDerKarte(gez[i]) == wahrerWertDerKarte(gez[j]))
				{
					c = wahrerWertDerKarte(gez[i]);
					
					for(int d = 0; d<gez.length-1;d++)
					{
						for(int f = d+1; f<gez.length;f++)
						{
							if(wahrerWertDerKarte(gez[d]) == wahrerWertDerKarte(gez[f]) &&
							   wahrerWertDerKarte(gez[d])!= c)
							{
								return true;
							}
						}
					}
					
					
				}
			}
		}
		return false;
	}
	public static boolean drilling(int gez[])
	{
		for(int i = 0; i<gez.length-2;i++)
		{
			for(int j = i+1; j<gez.length-1;j++)
			{
				for(int x = i+2; x<gez.length;x++)
				{
					if(wahrerWertDerKarte(gez[i]) == wahrerWertDerKarte(gez[j]) && wahrerWertDerKarte(gez[j]) == wahrerWertDerKarte(gez[x]))
					{
						return true;
						
					}
				}
			}
		}
		return false;
	}
	public static boolean vierling(int gez[])
	{
		for(int i = 0; i<gez.length-3;i++)
		{
			for(int j = i+1;j<gez.length-2;j++)
			{
				for(int x = i+2; x<gez.length-1;x++)
				{
					for(int e = i+3; e<gez.length;e++)
					{
						if(wahrerWertDerKarte(gez[i]) == wahrerWertDerKarte(gez[j])
						   && wahrerWertDerKarte(gez[j]) == wahrerWertDerKarte(gez[x])
						   && wahrerWertDerKarte(gez[x]) == wahrerWertDerKarte(gez[e]))
						{
							return true;
						}
					}
				}
			}
		}
		return false;	
	}
	public static boolean flush(int gez[])
	{
		if((wahreFarbeDerKarte(gez[0]) == wahreFarbeDerKarte(gez[1])) && 
		   (wahreFarbeDerKarte(gez[1]) == wahreFarbeDerKarte(gez[2])) &&
		   (wahreFarbeDerKarte(gez[2]) == wahreFarbeDerKarte(gez[3])) &&
		   (wahreFarbeDerKarte(gez[3]) == wahreFarbeDerKarte(gez[4])))
		{
			return true;
		}
		
		return false;
	}
	public static boolean straight(int gez[])
	{
		return false;
	}
	public static boolean fullHouse(int gez[])
	{
		if(einPaar(gez) == true && drilling(gez) == true)
		{
			return true;
		}
		return false;
	}
	public static boolean straightFlush(int gez[])
	{
		if(flush(gez) == true && straight(gez) == true)
		{
			return true;
		}
		return false;
	}
	public static void ausgabe()
	{
		e = System.currentTimeMillis();
		System.out.println("Zeit f�r den Durchlauf: " + ((e-b)/1000) + " Sekunden");
		System.out.println("Ein Paar: " + z[0] + " " + "Prozent: " + ((z[0]*100)/ versuche) + "%");
		System.out.println("Zwei Paar: " + z[1] + " " + "Prozent: " + ((z[1]*100)/ versuche) + "%");
		System.out.println("Drilling: " + z[2] + " " + "Prozent: " + ((z[2]*100)/ versuche) + "%");
		System.out.println("Vierling: " + z[3] + " " + "Prozent: " + ((z[3]*100)/ versuche) + "%");
		System.out.println("Flush: " + z[4] + " " + "Prozent: " + ((z[4]*100)/ versuche) + "%");
		System.out.println("Full House: " + z[5] + " " + "Prozent: " + ((z[5]*100)/ versuche) + "%");
		System.out.println("Straight: " + z[6] + " " + "Prozent: " + ((z[6]*100)/ versuche) + "%");
		System.out.println("Straight Flush: " + z[7] + " " + "Prozent: " + ((z[7]*100)/ versuche) + "%");
		PokerDatenank.database();
		PokerDatenank.insert();
		PokerDatenank.check();
	}
	
	public static int[] getz()
	{
		return z;
	}
	public static int getversuche()
	{
		return versuche;
	}
}
