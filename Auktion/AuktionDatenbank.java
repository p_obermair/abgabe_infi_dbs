import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.Statement;

public class AuktionDatenbank {

	public static void main(String[] args) {
		
		gui_Auktion g1 = new gui_Auktion();
		g1.setVisible(true);
		tableBenutzer();
		tableProdukt();
		tableGebot();

	}

	public static void tableBenutzer() {
		Connection c = null;
		Statement stmt = null;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:C:/sqlite/AuktionDatenbank.db");
			stmt = c.createStatement();
			String sql = "CREATE TABLE IF NOT Exists Benutzer " + "(Benutzername String PRIMARY KEY NOT NULL,"
					+ "Vorname String NOT NULL, " + "Nachname String NOT NULL, " + "Passwort String NOT NULL, "
					+ "eMail String NOT NULL, " + "GBTag int NOT NULL, " + "GBWoche int NOT NULL, "
					+ "GBJahr int NOT NULL);";
			stmt.executeUpdate(sql);
			stmt.close();
			c.close();
		} catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}
	}

	public static void tableProdukt() {
		Connection c = null;
		Statement stmt = null;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:C:/sqlite/AuktionDatenbank.db");
			stmt = c.createStatement();
			String sql = "CREATE TABLE IF NOT Exists Produkt " + "(ProduktID intPRIMARY KEY NOT NULL,"
					+ "Produktbeschreibung String NOT NULL, " + "Preis int NOT NULL, " + "EinTag int NOT NULL, "
					+ "EinWoche int NOT NULL, " + "EinJahr int NOT NULL, " + "Auktionsdauer int NOT NULL," 
					+ "BenutzernameB String NOT NULL," 
					+ "FOREIGN KEY (BenutzernameB) references Benutzer(Benutzername));";
			stmt.executeUpdate(sql);
			stmt.close();
			c.close();
		} catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}
	}
	public static void tableGebot()
	{
		Connection c = null;
		Statement stmt = null;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:C:/sqlite/AuktionDatenbank.db");
			stmt = c.createStatement();
			String sql = "CREATE TABLE IF NOT Exists Gebot " + "(GebotID intPRIMARY KEY NOT NULL,"
					+ "DatumTag int NOT NULL, " + "DatumMonat int NOT NULL, " + "DatumJahr int NOT NULL, "
					+ "Hoehe int NOT NULL, " + "ProduktID int NOT NULL," + "Benutzername String NOT NULL," 
					+ "FOREIGN KEY (Benutzername) references Benutzer(Benutzername),"
					+ "FOREIGN KEY (ProduktID) references Produkt(ProduktID));";
			stmt.executeUpdate(sql);
			stmt.close();
			c.close();
		} catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}
		
	}

	public static void benutzerAnlegen(Benutzer benutzer) {
		Connection c = null;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:C:/sqlite/AuktionDatenbank.db");
			String sql = "INSERT INTO BENUTZER Values(?,?,?,?,?,?,?,?);";
			PreparedStatement stmt = c.prepareStatement(sql);
			stmt.setString(1, benutzer.getBenutzername());
			stmt.setString(2, benutzer.getVorname());
			stmt.setString(3, benutzer.getNachname());
			stmt.setString(4, benutzer.getPasswort());
			stmt.setString(5, benutzer.geteMail());
			stmt.setInt(6, benutzer.getGBTag());
			stmt.setInt(7, benutzer.getGBWoche());
			stmt.setInt(8, benutzer.getGBJahr());
			stmt.executeUpdate();
			stmt.close();
			c.close();
		} catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}

	}

	public static void produktAnlegen(Produkt produkt) {
		Connection c = null;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:C:/sqlite/AuktionDatenbank.db");
			String sql = "INSERT INTO Produkt Values(?,?,?,?,?,?,?);";
			PreparedStatement stmt = c.prepareStatement(sql);
			stmt.setInt(1, produkt.getProduktID());
			stmt.setString(2, produkt.getProduktbeschreibung());
			stmt.setInt(3, produkt.getPreis());
			stmt.setInt(4, produkt.getEinTag());
			stmt.setInt(5, produkt.getEinWoche());
			stmt.setInt(6, produkt.getEinJahr());
			stmt.setInt(7, produkt.getAuktionsdauer());
			stmt.executeUpdate();
			stmt.close();
			c.close();
		} catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}

	}
	public static void gebotAbgeben(int gebotID, int h�he, int abTag, int abWoche, int abJahr, String benutzername, int produktID) {
		Connection c = null;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:C:/sqlite/AuktionDatenbank.db");
			String sql = "INSERT INTO Gebot Values(?,?,?,?,?,?,?);";
			PreparedStatement stmt = c.prepareStatement(sql);
			stmt.setInt(1, gebotID);
			stmt.setInt(2, abTag);
			stmt.setInt(3, abWoche);
			stmt.setInt(4, abJahr);
			stmt.setInt(5, h�he);
			stmt.setString(6, benutzername);
			stmt.setInt(7, produktID);
			stmt.executeUpdate();
			stmt.close();
			c.close();
		} catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}

	}
}
