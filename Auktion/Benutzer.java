public class Benutzer {
	String Vorname;
	String Nachname;
	String Passwort;
	String Benutzername;
	String eMail;
	int GBTag;
	int GBWoche;
	int GBJahr;

	public Benutzer(String vorname, String nachname, String passwort, String benutzername, String eMail, int gBTag,
			int gBWoche, int gBJahr) {
		super();
		Vorname = vorname;
		Nachname = nachname;
		Passwort = passwort;
		Benutzername = benutzername;
		this.eMail = eMail;
		GBTag = gBTag;
		GBWoche = gBWoche;
		GBJahr = gBJahr;
	}

	public String getVorname() {
		return Vorname;
	}

	public void setVorname(String vorname) {
		Vorname = vorname;
	}

	public String getNachname() {
		return Nachname;
	}

	public void setNachname(String nachname) {
		Nachname = nachname;
	}

	public String getPasswort() {
		return Passwort;
	}

	public void setPasswort(String passwort) {
		Passwort = passwort;
	}

	public String getBenutzername() {
		return Benutzername;
	}

	public void setBenutzername(String benutzername) {
		Benutzername = benutzername;
	}

	public String geteMail() {
		return eMail;
	}

	public void seteMail(String eMail) {
		this.eMail = eMail;
	}

	public int getGBTag() {
		return GBTag;
	}

	public void setGBTag(int gBTag) {
		GBTag = gBTag;
	}

	public int getGBWoche() {
		return GBWoche;
	}

	public void setGBWoche(int gBWoche) {
		GBWoche = gBWoche;
	}

	public int getGBJahr() {
		return GBJahr;
	}

	public void setGBJahr(int gBJahr) {
		GBJahr = gBJahr;
	}

}
