public class Produkt {
	int ProduktID;
	String Produktbeschreibung;
	int Preis;
	int EinTag;
	int EinWoche;
	int EinJahr;
	int Auktionsdauer;

	public Produkt(int produktID, String produktbeschreibung, int preis, int einTag, int einWoche, int einJahr,
			int auktionsdauer) {
		super();
		ProduktID = produktID;
		Produktbeschreibung = produktbeschreibung;
		Preis = preis;
		EinTag = einTag;
		EinWoche = einWoche;
		EinJahr = einJahr;
		Auktionsdauer = auktionsdauer;
	}

	public int getProduktID() {
		return ProduktID;
	}

	public void setProduktID(int produktID) {
		ProduktID = produktID;
	}

	public String getProduktbeschreibung() {
		return Produktbeschreibung;
	}

	public void setProduktbeschreibung(String produktbeschreibung) {
		Produktbeschreibung = produktbeschreibung;
	}

	public int getPreis() {
		return Preis;
	}

	public void setPreis(int preis) {
		Preis = preis;
	}

	public int getEinTag() {
		return EinTag;
	}

	public void setEinTag(int einTag) {
		EinTag = einTag;
	}

	public int getEinWoche() {
		return EinWoche;
	}

	public void setEinWoche(int einWoche) {
		EinWoche = einWoche;
	}

	public int getEinJahr() {
		return EinJahr;
	}

	public void setEinJahr(int einJahr) {
		EinJahr = einJahr;
	}

	public int getAuktionsdauer() {
		return Auktionsdauer;
	}

	public void setAuktionsdauer(int auktionsdauer) {
		Auktionsdauer = auktionsdauer;
	}

}
