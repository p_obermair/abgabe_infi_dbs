
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

public class gui_Auktion extends JFrame {
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField txtNachname;
	private JTextField txtVorname;
	private JTextField txtBenutzername;
	private JTextField txtEmail;
	private JPasswordField txtPasswort;
	private JTextField txtTagBenutzer;
	private JTextField txtProBeschreibung;
	private JTextField txtPreis;
	private JTextField txtTagProdukt;
	private JTextField txtAuktionsdauer;
	private JTextField txtTagGbot;
	private JTextField txtMonatGebot;
	private JTextField txtJahrGebot;
	private JTextField txtH�he;
	private JTextField txtBenutzernameGebot;
	private JTextField txtProduktIDGebot;
	private JTextField txtMonatProdukt;
	private JTextField txtJahrProdukt;
	private JTextField txtMonatBenutzer;
	private JTextField txtJahrBenutzer;

	/**
	 * Launch the application.
	 */
	
	Benutzer a;
	Produkt p;

	/**
	 * Create the frame.
	 */
	public gui_Auktion() {
		AuktionDatenbank d = new AuktionDatenbank();
		setTitle("Auktion");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 867, 630);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Benutzer anlegen:");
		lblNewLabel.setFont(new Font("Calibri", Font.PLAIN, 18));
		lblNewLabel.setBounds(10, 11, 166, 23);
		contentPane.add(lblNewLabel);
		
		txtNachname = new JTextField();
		txtNachname.setBounds(84, 87, 86, 20);
		contentPane.add(txtNachname);
		txtNachname.setColumns(10);
		
		txtVorname = new JTextField();
		txtVorname.setFont(new Font("Calibri", Font.PLAIN, 14));
		txtVorname.setBounds(74, 53, 86, 20);
		contentPane.add(txtVorname);
		txtVorname.setColumns(10);
		
		txtBenutzername = new JTextField();
		txtBenutzername.setBounds(105, 149, 86, 20);
		contentPane.add(txtBenutzername);
		txtBenutzername.setColumns(10);
		
		txtEmail = new JTextField();
		txtEmail.setBounds(74, 180, 86, 20);
		contentPane.add(txtEmail);
		txtEmail.setColumns(10);
		
		txtTagBenutzer = new JTextField();
		txtTagBenutzer.setFont(new Font("Calibri", Font.PLAIN, 14));
		txtTagBenutzer.setBounds(138, 211, 86, 20);
		contentPane.add(txtTagBenutzer);
		txtTagBenutzer.setColumns(10);
		
		txtPasswort = new JPasswordField();
		txtPasswort.setFont(new Font("Calibri", Font.PLAIN, 14));
		txtPasswort.setBounds(74, 118, 86, 20);
		contentPane.add(txtPasswort);
		
		JLabel lblNewLabel_1 = new JLabel("Vorname:");
		lblNewLabel_1.setFont(new Font("Calibri", Font.PLAIN, 14));
		lblNewLabel_1.setBounds(10, 52, 106, 23);
		contentPane.add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("Nachname:");
		lblNewLabel_2.setFont(new Font("Calibri", Font.PLAIN, 14));
		lblNewLabel_2.setBounds(10, 86, 113, 23);
		contentPane.add(lblNewLabel_2);
		
		JLabel lblPasswort = new JLabel("Passwort:");
		lblPasswort.setFont(new Font("Calibri", Font.PLAIN, 14));
		lblPasswort.setBounds(10, 118, 91, 23);
		contentPane.add(lblPasswort);
		
		JLabel lblNewLabel_3 = new JLabel("Benutzername:");
		lblNewLabel_3.setFont(new Font("Calibri", Font.PLAIN, 14));
		lblNewLabel_3.setBounds(10, 152, 106, 14);
		contentPane.add(lblNewLabel_3);
		
		JLabel lblNewLabel_4 = new JLabel("E-Mail:");
		lblNewLabel_4.setFont(new Font("Calibri", Font.PLAIN, 14));
		lblNewLabel_4.setBounds(10, 183, 46, 14);
		contentPane.add(lblNewLabel_4);
		
		JLabel lblGeburtsdatum = new JLabel("Geburtsdatum:");
		lblGeburtsdatum.setFont(new Font("Calibri", Font.PLAIN, 14));
		lblGeburtsdatum.setBounds(10, 211, 86, 23);
		contentPane.add(lblGeburtsdatum);
		
		JLabel lblNewLabel_5 = new JLabel("Produkt anlegen:");
		lblNewLabel_5.setFont(new Font("Calibri", Font.PLAIN, 18));
		lblNewLabel_5.setBounds(311, 11, 157, 23);
		contentPane.add(lblNewLabel_5);
		
		JLabel lblNewLabel_6 = new JLabel("ProBeschreibung:");
		lblNewLabel_6.setFont(new Font("Calibri", Font.PLAIN, 14));
		lblNewLabel_6.setBounds(311, 52, 157, 23);
		contentPane.add(lblNewLabel_6);
		
		txtProBeschreibung = new JTextField();
		txtProBeschreibung.setFont(new Font("Calibri", Font.PLAIN, 14));
		txtProBeschreibung.setBounds(417, 53, 353, 20);
		contentPane.add(txtProBeschreibung);
		txtProBeschreibung.setColumns(10);
		
		JLabel lblPreis = new JLabel("Preis:");
		lblPreis.setFont(new Font("Calibri", Font.PLAIN, 14));
		lblPreis.setBounds(311, 90, 46, 14);
		contentPane.add(lblPreis);
		
		txtPreis = new JTextField();
		txtPreis.setFont(new Font("Calibri", Font.PLAIN, 14));
		txtPreis.setBounds(352, 87, 86, 20);
		contentPane.add(txtPreis);
		txtPreis.setColumns(10);
		
		JLabel lblEinstelldatum = new JLabel("Einstelldatum:");
		lblEinstelldatum.setFont(new Font("Calibri", Font.PLAIN, 14));
		lblEinstelldatum.setBounds(311, 119, 86, 19);
		contentPane.add(lblEinstelldatum);
		
		txtTagProdukt = new JTextField();
		txtTagProdukt.setFont(new Font("Calibri", Font.PLAIN, 14));
		txtTagProdukt.setBounds(429, 118, 86, 20);
		contentPane.add(txtTagProdukt);
		txtTagProdukt.setColumns(10);
		
		JLabel lblAuktionsdauer = new JLabel("Auktionsdauer:");
		lblAuktionsdauer.setFont(new Font("Calibri", Font.PLAIN, 14));
		lblAuktionsdauer.setBounds(311, 152, 116, 14);
		contentPane.add(lblAuktionsdauer);
		
		txtAuktionsdauer = new JTextField();
		txtAuktionsdauer.setFont(new Font("Calibri", Font.PLAIN, 14));
		txtAuktionsdauer.setBounds(400, 149, 86, 20);
		contentPane.add(txtAuktionsdauer);
		txtAuktionsdauer.setColumns(10);
		
		JLabel lblGebotAbgeben = new JLabel("Gebot abgeben:");
		lblGebotAbgeben.setFont(new Font("Calibri", Font.PLAIN, 18));
		lblGebotAbgeben.setBounds(1, 352, 136, 23);
		contentPane.add(lblGebotAbgeben);
		
		JLabel lblDatum = new JLabel("Datum:");
		lblDatum.setFont(new Font("Calibri", Font.PLAIN, 14));
		lblDatum.setBounds(10, 395, 46, 14);
		contentPane.add(lblDatum);
		
		txtTagGbot = new JTextField();
		txtTagGbot.setFont(new Font("Calibri", Font.PLAIN, 14));
		txtTagGbot.setBounds(105, 392, 86, 20);
		contentPane.add(txtTagGbot);
		txtTagGbot.setColumns(10);
		
		JLabel lblTag = new JLabel("Tag:");
		lblTag.setFont(new Font("Calibri", Font.PLAIN, 14));
		lblTag.setBounds(66, 395, 46, 14);
		contentPane.add(lblTag);
		
		JLabel lblMonat = new JLabel("Monat:");
		lblMonat.setFont(new Font("Calibri", Font.PLAIN, 14));
		lblMonat.setBounds(201, 395, 46, 14);
		contentPane.add(lblMonat);
		
		txtMonatGebot = new JTextField();
		txtMonatGebot.setFont(new Font("Calibri", Font.PLAIN, 14));
		txtMonatGebot.setBounds(248, 392, 86, 20);
		contentPane.add(txtMonatGebot);
		txtMonatGebot.setColumns(10);
		
		JLabel lblJahr = new JLabel("Jahr:");
		lblJahr.setFont(new Font("Calibri", Font.PLAIN, 14));
		lblJahr.setBounds(352, 395, 46, 14);
		contentPane.add(lblJahr);
		
		txtJahrGebot = new JTextField();
		txtJahrGebot.setFont(new Font("Calibri", Font.PLAIN, 14));
		txtJahrGebot.setBounds(382, 392, 86, 20);
		contentPane.add(txtJahrGebot);
		txtJahrGebot.setColumns(10);
		
		JLabel lblHhe = new JLabel("H\u00F6he:");
		lblHhe.setFont(new Font("Calibri", Font.PLAIN, 14));
		lblHhe.setBounds(10, 420, 46, 14);
		contentPane.add(lblHhe);
		
		txtH�he = new JTextField();
		txtH�he.setFont(new Font("Calibri", Font.PLAIN, 14));
		txtH�he.setBounds(47, 417, 86, 20);
		contentPane.add(txtH�he);
		txtH�he.setColumns(10);
		
		JLabel lblBenutzername = new JLabel("Benutzername:");
		lblBenutzername.setFont(new Font("Calibri", Font.PLAIN, 14));
		lblBenutzername.setBounds(10, 445, 147, 14);
		contentPane.add(lblBenutzername);
		
		txtBenutzernameGebot = new JTextField();
		txtBenutzernameGebot.setFont(new Font("Calibri", Font.PLAIN, 14));
		txtBenutzernameGebot.setBounds(105, 442, 86, 20);
		contentPane.add(txtBenutzernameGebot);
		txtBenutzernameGebot.setColumns(10);
		
		JLabel lblProduktid = new JLabel("ProduktID:");
		lblProduktid.setFont(new Font("Calibri", Font.PLAIN, 14));
		lblProduktid.setBounds(10, 470, 127, 14);
		contentPane.add(lblProduktid);
		
		txtProduktIDGebot = new JTextField();
		txtProduktIDGebot.setFont(new Font("Calibri", Font.PLAIN, 14));
		txtProduktIDGebot.setBounds(74, 467, 86, 20);
		contentPane.add(txtProduktIDGebot);
		txtProduktIDGebot.setColumns(10);
		
		JLabel lblTag_1 = new JLabel("Tag:");
		lblTag_1.setFont(new Font("Calibri", Font.PLAIN, 14));
		lblTag_1.setBounds(400, 121, 46, 14);
		contentPane.add(lblTag_1);
		
		JLabel lblMonat_1 = new JLabel("Monat:");
		lblMonat_1.setFont(new Font("Calibri", Font.PLAIN, 14));
		lblMonat_1.setBounds(525, 121, 46, 14);
		contentPane.add(lblMonat_1);
		
		txtMonatProdukt = new JTextField();
		txtMonatProdukt.setFont(new Font("Calibri", Font.PLAIN, 14));
		txtMonatProdukt.setBounds(575, 118, 86, 20);
		contentPane.add(txtMonatProdukt);
		txtMonatProdukt.setColumns(10);
		
		JLabel lblJahr_1 = new JLabel("Jahr:");
		lblJahr_1.setFont(new Font("Calibri", Font.PLAIN, 14));
		lblJahr_1.setBounds(671, 121, 46, 14);
		contentPane.add(lblJahr_1);
		
		txtJahrProdukt = new JTextField();
		txtJahrProdukt.setBounds(703, 118, 86, 20);
		contentPane.add(txtJahrProdukt);
		txtJahrProdukt.setColumns(10);
		
		JLabel lblNewLabel_7 = new JLabel("Tag:");
		lblNewLabel_7.setFont(new Font("Calibri", Font.PLAIN, 14));
		lblNewLabel_7.setBounds(105, 215, 46, 14);
		contentPane.add(lblNewLabel_7);
		
		JLabel lblMonat_2 = new JLabel("Monat:");
		lblMonat_2.setFont(new Font("Calibri", Font.PLAIN, 14));
		lblMonat_2.setBounds(105, 242, 46, 14);
		contentPane.add(lblMonat_2);
		
		txtMonatBenutzer = new JTextField();
		txtMonatBenutzer.setFont(new Font("Calibri", Font.PLAIN, 14));
		txtMonatBenutzer.setBounds(161, 239, 86, 20);
		contentPane.add(txtMonatBenutzer);
		txtMonatBenutzer.setColumns(10);
		
		JLabel lblJahr_2 = new JLabel("Jahr:");
		lblJahr_2.setFont(new Font("Calibri", Font.PLAIN, 14));
		lblJahr_2.setBounds(105, 267, 46, 14);
		contentPane.add(lblJahr_2);
		
		txtJahrBenutzer = new JTextField();
		txtJahrBenutzer.setFont(new Font("Calibri", Font.PLAIN, 14));
		txtJahrBenutzer.setBounds(138, 264, 86, 20);
		contentPane.add(txtJahrBenutzer);
		txtJahrBenutzer.setColumns(10);
		
		JButton btnBenutzerAnlegen = new JButton("Benutzer anlegen:");
		btnBenutzerAnlegen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) 
			{
				String a1 = txtBenutzername.getText();
				String a2 = txtVorname.getText();
				String a3 = txtNachname.getText();
				@SuppressWarnings("deprecation")
				String a4 = txtPasswort.getText();
				String a5 = txtEmail.getText();
				int a6 = Integer.parseInt(txtTagBenutzer.getText());
				int a7 = Integer.parseInt(txtMonatBenutzer.getText());
				int a8 = Integer.parseInt(txtJahrBenutzer.getText());
				
				a = new Benutzer(a1,a2,a3,a4,a5,a6,a7,a8);
				AuktionDatenbank.benutzerAnlegen(a);
				
			}
		});
		btnBenutzerAnlegen.setBounds(64, 292, 160, 23);
		contentPane.add(btnBenutzerAnlegen);
		
		JButton btnGebotAbgeben = new JButton("Gebot abgeben:");
		btnGebotAbgeben.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) 
			{
				int a1 = (int) ((Math.random()*10000000)+1);
				int a2 = Integer.parseInt(txtH�he.getText());
				int a3 = Integer.parseInt(txtTagGbot.getText());
				int a4 = Integer.parseInt(txtMonatGebot.getText());
				int a5 = Integer.parseInt(txtJahrGebot.getText());
				String a6 = txtBenutzernameGebot.getText();
				int a7 = Integer.parseInt(txtProduktIDGebot.getText());
				
				AuktionDatenbank.gebotAbgeben(a1,a2,a3,a4,a5,a6,a7);
				
			}
		});
		btnGebotAbgeben.setBounds(74, 511, 166, 23);
		contentPane.add(btnGebotAbgeben);
		
		JButton btnProduktAnlegen = new JButton("Produkt anlegen:");
		btnProduktAnlegen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) 
			{
				int a1 = (int) ((Math.random()*1000000000)+1);
				String a2 = txtProBeschreibung.getText();
				int a3 = Integer.parseInt(txtPreis.getText());
				int a4 = Integer.parseInt(txtTagProdukt.getText());
				int a5 = Integer.parseInt(txtMonatProdukt.getText());
				int a6 = Integer.parseInt(txtJahrProdukt.getText());
				int a7 = Integer.parseInt(txtAuktionsdauer.getText());
				
				p = new Produkt(a1,a2,a3,a4,a5,a6,a7);
				AuktionDatenbank.produktAnlegen(p);
				
			}
		});
		btnProduktAnlegen.setBounds(495, 183, 136, 23);
		contentPane.add(btnProduktAnlegen);
		
		
	}
}
