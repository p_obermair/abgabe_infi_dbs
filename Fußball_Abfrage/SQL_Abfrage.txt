SELECT vereinname, 
(SELECT sum(toreHeim) FROM Spiel sh WHERE sh.heimmannschaft = t.vereinsname),
(SELECT sum(toreGast) FROM Spiel sg WHERE sg.gastmannschaft = t.vereinsname)
FROM Team t

Welcher Spieler spielt hat wann f�r welche Mannschaft gespielt?
SELECT vorname, nachname, t.vereinsname, von, bis
FROM Team t, SpieltF�r sf, Spieler s
WHERE sf.spielerPassNr = s.spielerPassNr
AND t.verinsname = sf.vereinsname ORDER BY vorname;

Das Team mit den meisten roten Karten?
SELECT t.vereinsname, COUNT(*) AS ar
FROM Team t, SpieltF�r sf, Spieler s, Karten k
WHERE t.verinsname = sf.vereinsname 
AND sf.spielerPassNr = s.spielerPassNr
AND s.spielerPassNr = k.spielerPassNR
AND k.farbe = 'Rot'
GROUP BY t.vereinsname 
HAVING ar >= ALL 
(SELECT COUNT(*)
FROM Team t1, SpieltF�r sf1, Spieler s1, Karten k1
WHERE t1.verinsname = sf1.vereinsname 
AND sf1.spielerPassNr = s1.spielerPassNr
AND s1.spielerPassNr = k1.spielerPassNR
AND k1.farbe = 'Rot'
GROUP BY t1.vereinsname);

In Welchen Stadion sind die meisten Karten gefallen?
SELECT s.stadionname, COUNT(*) AS ak
FROM Stadion s, Spiel sp, Karten k
WHERE s.stadionname = sp.stadion 
AND sp.spielID = k.spielID
GROUP BY s.stadionname
HAVING ak >= ALL
(SELECT COUNT(*)
FROM Stadion s1, Spiel sp1, Karten k1
WHERE s1.stadionname = sp1.stadion 
AND sp1.spielID = k1.spielID
GROUP BY s1.stadionname);


