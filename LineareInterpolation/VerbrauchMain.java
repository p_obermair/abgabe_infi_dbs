import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class VerbrauchMain {

	private static Connection con;

	public static void main(String[] args) throws SQLException {
		getCon();
		
		for(int i = 4; i <= 11; i++)
		{
			Date erster = new Date(2015,i,1);
			
			Verbrauch minStrom = VerbrauchMain.getFirstStrom(erster);
			Verbrauch maxStrom = VerbrauchMain.getLastStrom(erster);
			Verbrauch minWasser = VerbrauchMain.getFirstWasser(erster);
			Verbrauch maxWasser = VerbrauchMain.getLastWasser(erster);
			
			Date minStromD = minStrom.getD();
			Date maxStromD = maxStrom.getD();
			Date minWasserD = minWasser.getD();
			Date maxWasserD = maxWasser.getD();
			
			double strom = lineareInterpolation(minStrom, maxStrom, erster.getTime());
			double wasser = lineareInterpolation(minWasser, maxWasser, erster.getTime());
			
			Verbrauch finalStrom = new Verbrauch(erster,strom);
			Verbrauch finalWasser = new Verbrauch(erster, wasser);
			
			VerbrauchMain.verbaruchEinfuegenStrom(finalStrom);
			VerbrauchMain.verbaruchEinfuegenWasser(finalWasser);
		}
	}

	public static void getCon() throws SQLException {
		try {
			Class.forName("org.sqlite.JDBC");
			con = DriverManager.getConnection("jdbc:sqlite:C:/Schule/4AHWII/INFI-DBS/Verbrauch/Verbrauch.db");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	public static Verbrauch getFirstStrom(Date erster) {

		String sql = "SELECT MAX(datum),value FROM strom_verbrauch WHERE datum <= ?";
		PreparedStatement prestmt = null;
		ResultSet rs = null;

		try {
			prestmt = con.prepareStatement(sql);
			prestmt.setLong(1, erster.getTime() / 1000);
			rs = prestmt.executeQuery();

			rs.next();

			return new Verbrauch(new Date(rs.getLong(1) * 1000), rs.getDouble(2));

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static Verbrauch getLastStrom(Date erster) {

		String sql = "SELECT MIN(datum),value FROM strom_verbrauch WHERE datum > ?";
		PreparedStatement prestmt = null;
		ResultSet rs = null;

		try {
			prestmt = con.prepareStatement(sql);
			prestmt.setLong(1, erster.getTime() / 1000);
			rs = prestmt.executeQuery();

			rs.next();

			return new Verbrauch(new Date(rs.getLong(1) * 1000), rs.getDouble(2));

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	public static Verbrauch getFirstWasser(Date erster) {

		String sql = "SELECT MAX(datum),value FROM wasser_verbrauch WHERE datum <= ?";
		PreparedStatement prestmt = null;
		ResultSet rs = null;

		try {
			prestmt = con.prepareStatement(sql);
			prestmt.setLong(1, erster.getTime() / 1000);
			rs = prestmt.executeQuery();

			rs.next();

			return new Verbrauch(new Date(rs.getLong(1) * 1000), rs.getDouble(2));

		} catch (SQLException e) {
			e.printStackTrace();
		}
			return null;
	}

	public static Verbrauch getLastWasser(Date erster) {

		String sql = "SELECT MIN(datum),value FROM wasser_verbrauch WHERE datum > ?";
		PreparedStatement prestmt = null;
		ResultSet rs = null;

		try {
			prestmt = con.prepareStatement(sql);
			prestmt.setLong(1, erster.getTime() / 1000);
			rs = prestmt.executeQuery();

			rs.next();

			return new Verbrauch(new Date(rs.getLong(1) * 1000), rs.getDouble(2));

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}


	public static double lineareInterpolation(Verbrauch min, Verbrauch max, long erster ) {
		double k = (max.getWert() - min.getWert()) / (max.getD().getTime() - min.getD().getTime());
		double d = max.getWert() - max.getD().getTime() * k;
		double result = k * erster + d;

		return result;

	}
	public static void verbaruchEinfuegenStrom(Verbrauch verbrauch) {

		PreparedStatement prestmt = null;
		try {

			String sql = "INSERT INTO strom_verbrauch VALUES(?,?)";
			prestmt = con.prepareStatement(sql);
			prestmt.setLong(1, verbrauch.getD().getTime() / 1000);
			prestmt.setDouble(2, verbrauch.getWert());
			prestmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();

		} 
	}
	public static void verbaruchEinfuegenWasser(Verbrauch verbrauch) {

		PreparedStatement prestmt = null;
		try {

			String sql = "INSERT INTO wasser_verbrauch VALUES(?,?)";
			prestmt = con.prepareStatement(sql);
			prestmt.setLong(1, verbrauch.getD().getTime() / 1000);
			prestmt.setDouble(2, verbrauch.getWert());
			prestmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();

		} 
	}
}
